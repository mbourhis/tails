## Upgrades and changes

- Upgrade Tor Browser to [5.0.7](https://blog.torproject.org/blog/tor-browser-507-released)
