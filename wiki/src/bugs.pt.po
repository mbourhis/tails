# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2014-07-13 10:15+0300\n"
"PO-Revision-Date: 2014-06-22 08:38-0300\n"
"Last-Translator: Tails Developers <amnesia@boum.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "[[!meta title=\"bugs\"]]"
msgid "[[!meta title=\"Bugs\"]]\n"
msgstr "[[!meta title=\"bugs\"]]"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "If you've found a bug in The Amnesic Incognito Live System, please read "
#| "[[support/found_a_problem]]."
msgid ""
"If you've found a bug in Tails, please read [[doc/first_steps/"
"bug_reporting]]."
msgstr ""
"Se você encontrou um bug no Tails, por favor leia [[encontrei um problema|"
"support/found_a_problem]]. "

#. type: Plain text
#, fuzzy
#| msgid ""
#| "We don't use this section anymore, see [[!tails_redmine \"\" desc="
#| "\"Redmine\"]] instead."
msgid ""
"We don't use this section anymore, see [[contribute/working_together/"
"Redmine]] instead."
msgstr ""
"Nós não usamos mais esta seção. Veja a página do [[!tails_redmine \"\" desc="
"\"Redmine\"]]."
