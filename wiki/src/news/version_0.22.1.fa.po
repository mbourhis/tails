# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2015-07-28 09:53+0200\n"
"PO-Revision-Date: 2015-10-26 13:53+0000\n"
"Last-Translator: sprint5 <translation5@451f.org>\n"
"Language-Team: Persian "
"<http://weblate.451f.org:8889/projects/tails/version_0221/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.4-dev\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta date=\"Wed Feb 5 10:00:00 2014\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Tails 0.22.1 is out\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!tag announce]]\n"
msgstr "[[!tag announce]]\n"

#. type: Plain text
msgid "Tails, The Amnesic Incognito Live System, version 0.22.1, is out."
msgstr ""

#. type: Plain text
msgid ""
"All users must upgrade as soon as possible: this release fixes [[numerous "
"security issues|security/Numerous_security_holes_in_0.22]]."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=1]]\n"
msgstr "[[!toc levels=1]]\n"

#. type: Title #
#, no-wrap
msgid "Changes"
msgstr ""

#. type: Plain text
msgid "Notable user-visible changes include:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"* Security fixes\n"
"  - Upgrade the web browser to 24.3.0esr, that fixes a few serious\n"
"    security issues.\n"
"  - Upgrade the system NSS to 3.14.5, that fixes a few serious\n"
"    security issues.\n"
"  - Workaround a browser size fingerprinting issue by using small\n"
"    icons in the web browser's navigation toolbar.\n"
"  - Upgrade Pidgin to 2.10.8, that fixes a number of serious\n"
"    security issues.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"* Major improvements\n"
"  - Check for upgrades availability using Tails Upgrader, and propose\n"
"    to apply an incremental upgrade whenever possible.\n"
"  - Install Linux 3.12 (3.12.6-2).\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"* Bugfixes\n"
"  - Fix the keybindings problem introduced in 0.22.\n"
"  - Fix the Unsafe Browser problem introduced in 0.22.\n"
"  - Use IE's icon in Windows camouflage mode.\n"
"  - Handle some corner cases better in Tails Installer.\n"
"  - Use the correct browser homepage in Spanish locales.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"* Minor improvements\n"
"  - Update Torbutton to 1.6.5.3.\n"
"  - Do not start Tor Browser automatically, but notify when Tor\n"
"    is ready.\n"
"  - Import latest Tor Browser prefs.\n"
"  - Many user interface improvements in Tails Upgrader.\n"
msgstr ""

#. type: Plain text
msgid ""
"See the [online "
"Changelog](https://git-tails.immerda.ch/tails/plain/debian/changelog)  for "
"technical details."
msgstr ""

#. type: Title #
#, no-wrap
msgid "Known issues"
msgstr "مشکلات شناسایی‌شده"

#. type: Bullet: '* '
msgid ""
"The memory erasure on shutdown [[!tails_ticket 6460 desc=\"does not work on "
"some hardware\"]]."
msgstr ""
"پاک شدن حافظه هنگام خاموش شدن [[!tails_ticket 6460 desc=\"روی بعضی سخت‌"
"افزارها کار نمی‌کند\"]]."

#. type: Bullet: '* '
msgid "[[Longstanding|support/known_issues]] known issues."
msgstr ""

#. type: Title #
#, no-wrap
msgid "I want to try it or to upgrade!"
msgstr ""

#. type: Plain text
msgid "Go to the [[download]] page."
msgstr ""

#. type: Plain text
msgid ""
"As no software is ever perfect, we maintain a list of [[problems that "
"affects the last release of Tails|support/known_issues]]."
msgstr ""

#. type: Title #
#, no-wrap
msgid "What's coming up?"
msgstr ""

#. type: Plain text
msgid "The next Tails release is [[scheduled|contribute/calendar]] for March 18."
msgstr ""

#. type: Plain text
msgid "Have a look to our [[!tails_roadmap]] to see where we are heading to."
msgstr ""

#. type: Plain text
msgid ""
"Would you want to help? There are many ways [[**you** can contribute to "
"Tails|contribute]]. If you want to help, come talk to us!"
msgstr ""
