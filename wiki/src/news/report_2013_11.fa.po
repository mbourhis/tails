# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2015-07-28 09:53+0200\n"
"PO-Revision-Date: 2015-10-22 14:26+0000\n"
"Last-Translator: sprint5 <translation5@451f.org>\n"
"Language-Team: Persian "
"<http://weblate.451f.org:8889/projects/tails/report_2013_11/fa/>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.4-dev\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Tails report for November, 2013\"]]\n"
msgstr "[[!meta title=\"گزارش نوامبر ۲۰۱۳ تیلز\"]]\n"

#. type: Title =
#, no-wrap
msgid "Metrics\n"
msgstr "متریک‌ها\n"

#. type: Bullet: '- '
msgid ""
"Tails has been started more than 205 802 times in November.  This make 6 860 "
"boots a day in average."
msgstr ""
"تیلز بیش از ۲۰۵٫۸۰۲  بار در ماه نوامبر راه‌اندازی شد. یعنی به طور متوسط ۶٫"
"۸۶۰ راه‌اندازی در روز."

#. type: Bullet: '- '
msgid "6 270 downloads of the OpenPGP signature of Tails ISO."
msgstr "- ۶٫۲۷۰ دانلود امضای اُپن‌پی‌جی‌پی ایزوی تیلز."

#. type: Bullet: '- '
msgid "80 reports were received through WhisperBack."
msgstr "۸۰ گزارش از طریق ویسپربک دریافت شدند."

#. type: Title =
#, no-wrap
msgid "Code\n"
msgstr "کد\n"

#. type: Plain text
msgid "Some of the newest contributors were particularly active this month:"
msgstr "بعضی از جدیدترین مشارکت‌کنندگان این ماه بسیار فعال بودند:"

#. type: Bullet: '  - '
msgid ""
"We have a new and active maintainer for I2P in Tails. Big up to Kytv! So I2P "
"was [[!tails_gitweb_branch feature/i2p-0.9.8.1 desc=\"updated\"]] to "
"0.9.8.1, and the I2P configuration and integration was improved in several "
"ways."
msgstr ""
"حالا یک مسئول نگهداری جدید و فعال برای I2P در تیلز داریم. تشکر بسیار از "
"Kytv! بنابراین I2P به ۰٫۹٫۸٫۱ [[!tails_gitweb_branch feature/i2p-0.9.8.1 "
"desc=\"ارتقاء\"]] پیدا کرد و پیکربندی و هم‌سازی I2P از چندین راه بهبود "
"یافتند."

#. type: Bullet: '  - '
msgid ""
"SCIM was "
"[replaced](http://git.tails.boum.org/winterfairy/tails/log/?h=bugfix/ibus)  "
"by IBus as input method to better support various input methods (Closes "
"[[!tails_ticket 5624]] and [[!tails_ticket 6206]]). Thanks to WinterFairy!"
msgstr ""
"IBus "
"[جایگزین](http://git.tails.boum.org/winterfairy/tails/log/?h=bugfix/ibus) "
"SCIM به عنوان روش ورودی شد تا از روش‌های ورودی متعدد بهتر پشتیبانی شود (بسته "
"شدن [[!tails_ticket 5624]] و [[!tails_ticket 6206]]). تشکر بسیار از "
"WinterFairy!"

#. type: Bullet: '  - '
msgid ""
"Kevin C. Krinke has been actively working on a [replacement of the "
"clock](http://kevin.c.krinke.ca/builds/tailsclock) with configurable "
"timezone. Many thanks!"
msgstr ""
"Kevin C. Krinke فعالانه مشغول [جایگزینی "
"ساعت](http://kevin.c.krinke.ca/builds/tailsclock) با زمان قابل‌تنظیم بوده‌"
"است. تشکر بسیار!"

#. type: Bullet: '  - '
msgid ""
"A script has been written to automate the [[!tails_gitweb_branch "
"feature/import-translations desc=\"importation process\"]] of completed "
"translations from Transifex. Thanks to WinterFairy!"
msgstr ""
"اسکریپتی برای خودکارسازی [[!tails_gitweb_branch feature/import-translations "
"desc=\"روند وارد کردن\"]] ترجمه‌های تمام‌شده از Transifex نوشته شد. تشکر "
"بسیار از WinterFairy!"

#. type: Bullet: '  - '
msgid ""
"The keyboard selection for some languages in Tails Greeter was "
"[fixed](http://git.tails.boum.org/greeter/log/?h=bugfix/5469-default-country-and-translation)  "
"(Closes [[!tails_ticket 5741]]). Again thanks to WinterFairy!"
msgstr ""
"مشکل انتخاب صفحه‌کلید برای بعضی زبان‌ها در خوشامدگوی تیلز [حل "
"شد](http://git.tails.boum.org/greeter/log/?h=bugfix/5469-default-country-and-"
"translation)   (بسته شدن [[!tails_ticket 5741]]). باز هم تشکر از WintwrFairy!"

#. type: Plain text
msgid "We kept on working on some major features, some are about to be released:"
msgstr ""
"به کار روی بعضی ویژگی‌های عمده ادامه دادیم و بعضی از آن‌ها به زودی منتشر "
"می‌شوند:"

#. type: Bullet: '  - '
msgid ""
"The real-world deployment of incremental updates has been "
"[[!tails_gitweb_branch feature/incremental-upgrades desc=\"made ready for "
"beta testing\"]] and will be the default way to upgrade Tails, on "
"point-releases at least."
msgstr ""
"ارتقاهای تدریجی حالا [[!tails_gitweb_branch feature/incremental-upgrades "
"desc=\"برای آزمایش کردن در حالت بتا آماده شده\"]] و روش پیش‌فرض ارتقای تیلز "
"خواهد بود، اقلاً برای ارتقاهای جزیی."

#. type: Bullet: '  - '
msgid ""
"The second round of upgrade towards a [[safer "
"persistence|doc/first_steps/persistence/upgrade]] was [[!tails_gitweb_branch "
"bugfix/safer-persistence desc=\"prepared\"]]."
msgstr ""
"دور دوم ارتقاء به یک [مانای امن‌تر|doc/first_steps/persistence/upgrade]] [["
"!tails_gitweb_branch bugfix/safer-persistence desc=\"آماده شد\"]]."

#. type: Bullet: '  - '
msgid ""
"A lot of work has been done towards [[!tails_gitweb_branch feature/spoof-mac "
"desc=\"MAC spoofing\"]]. See the latest [[blueprint|blueprint/macchanger]] "
"of our design."
msgstr ""
"کارهای زیادی برای [[!tails_gitweb_branch feature/spoof-mac desc=\"جا زدن "
"آدرس مک\"]] انجام شده‌اند. جدیدترین [[بلوپرینت|blueprint/macchanger]] طراحی "
"ما را ببینید."

#. type: Bullet: '  - '
msgid ""
"The migration to Firefox 24 and Torbutton 1.6 was completed (Closes "
"[[!tails_ticket 6370]] and [[!tails_ticket 6371]])."
msgstr ""
"کوچ به فایرفاکس ۲۴ و تورباتن ۱٫۶ کامل شد (بسته شدن [[!tails_ticket 6370]] و[["
"!tails_ticket 6371]])."

#. type: Plain text
msgid "Regarding security:"
msgstr "در مورد امنیت:"

#. type: Bullet: '  - '
msgid ""
"The serious security upgrade to NSS 3.15.3 was prepared but we unfortunately "
"didn't have time to make a point release just for that. See CVE-2013-1741, "
"CVE-2013-5605 and CVE-2013-5606."
msgstr ""
"ارتقای امنیتی مهم به NSS 3.15.3 آماده شد، اما متأسفانه فرصتی برای یک انتشار "
"جزیی تنها برای همین کار نداشتیم. رجوع کنید به CVE-2013-1741، CVE-2013-5605 و "
"CVE-2013-5606."

#. type: Bullet: '  - '
msgid ""
"More IP and MAC addresses are sanitized in WhisperBack reports (Closes "
"[[!tails_ticket 6391]])."
msgstr ""
"آی‌پی‌ها و آدرس‌های مک بیشتری در گزارش های ویسپربک درست شدند (بسته شدن [["
"!tails_ticket 6391]])."

#. type: Plain text
msgid "Regarding user experience:"
msgstr "در مورد تجربهٔ کاربر:"

#. type: Bullet: '  - '
msgid ""
"DPMS screen blanking has been [[!tails_gitweb_branch bugfix/disable-dpms "
"desc=\"disabled\"]] (Closes [[!tails_ticket 5617]])."
msgstr ""
"خالی کردن صفحه از سوی DPMS  [[!tails_gitweb_branch bugfix/disable-dpms desc="
"\"غیرفعال شده‌است\"]] (بسته شدن [[!tails_ticket 5617]])."

#. type: Bullet: '  - '
msgid ""
"The default system partition size is now [[!tails_gitweb_branch "
"feature/bigger-system-partition desc=\"2.5GB\"]]."
msgstr ""
"گنجایش پیش‌فرض پارتیشن سیستم حالا [[!tails_gitweb_branch feature/bigger-"
"system-partition desc=\"۲٫۵ گیگابایت است\"]]."

#. type: Bullet: '  - '
msgid ""
"The bug that prevented Vidalia to show up sometimes was "
"[[!tails_gitweb_branch bugfix/vidalia_fails_to_start desc=\"fixed\"]]."
msgstr ""
"ایرادی که گاهی از باز شدن ویدالیا جلوگیری می‌کرد [[!tails_gitweb_branch "
"bugfix/vidalia_fails_to_start desc=\"برطرف شده‌است\"]]."

#. type: Plain text
msgid "And also:"
msgstr "همچنین:"

#. type: Bullet: '  - '
msgid ""
"Tails now have its own [[!tails_gitweb "
"config/chroot_local-includes/usr/local/lib/tails-shell-library "
"desc=\"shell\"]] and [perl](https://git-tails.immerda.ch/perl5lib) libraries "
"where we factorize some of our code."
msgstr ""
"تیلز حالا کتابخانه‌های [[!tails_gitweb config/chroot_local-"
"includes/usr/local/lib/tails-shell-library desc=\"هسته\"]] و [پرل](https"
"://git-tails.immerda.ch/perl5lib) خود را دارد که در آن‌ها بخشی از کد خود را "
"فاکتور می‌گیریم."

#. type: Bullet: '  - '
msgid ""
"Upgrade to Linux 3.11 and got back to Linux 3.10 because 3.11 breaks the "
"shutdown process on some hardware."
msgstr ""
"به لینوکس ۳٫۱۱ ارتقاء پیدا کردیم و سپس به لینوکس ۳٫۱۰ بازگشتیم، چون ۳٫۱۱ "
"روند خاموش کردن را روی بعضی سخت‌افزارها متوقف می‌کند."

#. type: Bullet: '  - '
msgid "Uploaded a few packages and backports we need into Debian."
msgstr "چند بسته و بک‌پورت مورد نیاز خود را در دبیان آپلود کردیم."

#. type: Bullet: '  - '
msgid ""
"Tails Installer [[!tails_gitweb_branch "
"bugfix/dont-fail-upgrade-if-tmp-dir-exists-on-destination desc=\"does not "
"fail anymore\"]] if the `tmp` directory exists on the destination "
"filesystem."
msgstr ""
"نصب‌کنندهٔ تیلز در صورت وجود پوشهٔ `tmp` روی فایل سیستمی مقصد [["
"!tails_gitweb_branch bugfix/dont-fail-upgrade-if-tmp-dir-exists-on-"
"destination desc=\"دیگر متوقف نمی‌شود\"]]."

#. type: Title =
#, no-wrap
msgid "Documentation and website\n"
msgstr "مستندسازی و تارنما\n"

#. type: Bullet: '  - '
msgid ""
"A [[draft FAQ|blueprint/faq]] was create to answer some questions asked on "
"our previous [[forum|forum]]."
msgstr ""
"[[پیش‌نویس پرسش و پاسخ‌های متداول|blueprint/faq]] برای پاسخ‌گویی به بعضی "
"سوالات پرسیده‌شده در [[انجمن|forum]] قبلی ایجاد شد."

#. type: Bullet: '  - '
msgid ""
"A [[calendar|contribute/calendar]] was created to centralize all the "
"important dates for the community."
msgstr ""
"یک [[تقویم|contribute/calendar]] برای مرکزی‌سازی تمام تاریخ‌های مهم اجتماع "
"ایجاد شد."

#. type: Bullet: '  - '
msgid ""
"A [[glossary|contribute/glossary]] was created on the website to define "
"terms specific to our process."
msgstr ""
"[[واژه‌نامه‌ای|contribute/glossary]] روی تارنما برای تعریف اصطلاحات مخصوص "
"روندهای ما ایجاد شد."

#. type: Bullet: '  - '
msgid "The roles on the Contribute page were [[described|contribute]]."
msgstr "نقش صفحهٔ همکاری [[دوباره تعیین شد|contribute]]."

#. type: Bullet: '  - '
msgid ""
"Two new contribution pages were added: [[graphics|contribute/how/graphics]] "
"and [[user_interface|contribute/how/user_interface]]."
msgstr ""
"دو صفحهٔ جدید همکاری اضافه شدند: [[graphics|contribute/how/graphics]] و "
"[[user_interface|contribute/how/user_interface]]."

#. type: Bullet: '  - '
msgid ""
"Instructions to change the persistence passphrase were "
"[[!tails_gitweb_branch doc/change_persistence_passphrase desc=\"drafted\"]]."
msgstr ""
"راهنمای تغییر گذرواژهٔ مانا [[!tails_gitweb_branch doc/"
"change_persistence_passphrase desc=\"پیش‌نویس شد\"]]."

#. type: Bullet: '  - '
msgid ""
"The [[contribute/release_schedule]] was updated to match current practice "
"more closely."
msgstr ""
"[[contribute/release_schedule]] به‌روز شد تا بیشتر به کارهای اخیر نزدیک باشد."

#. type: Bullet: '  - '
msgid ""
"The [[documentation for translators|contribute/how/translate]] now points to "
"Git tutorials."
msgstr ""
"[[مستندات مترجم‌ها|contribute/how/translate]] حالا به راهنماهای گیت اشاره "
"می‌کند."

#. type: Title =
#, no-wrap
msgid "Infrastructure\n"
msgstr "زیرساخت\n"

#. type: Bullet: '  - '
msgid "We have organized donations of UEFI hardware, and purchased some more."
msgstr "چند سخت‌افزار UEFI به ما داده شد و چند تای دیگر خریداری کردیم."

#. type: Bullet: '  - '
msgid "Recovered from disk breakage on our main server."
msgstr "مشکل دیسک روی سرور اصلی خود را برطرف کردیم."

#. type: Bullet: '  - '
msgid ""
"Surveyed operators of HTTP mirrors for SNI availability.  The results look "
"good, next step is to get a certificate for `dl.amnesia.boum.org`."
msgstr ""
"اپراتورهای آینه‌های اچ‌تی‌تی‌پی را جهت مطلع شدن از در دسترس بودن SNI بررسی "
"کردیم. نتیجه خوب بود. قدم بعدی گرفتن یک گواهی برای `dl.amnesia.boum.org` است."

#. type: Title =
#, no-wrap
msgid "On-going discussions\n"
msgstr "بحث‌های دنباله‌دار\n"

#. type: Bullet: '  - '
msgid ""
"[2.0 milestone += supporting USB devices exposed as "
"non-removable?](https://mailman.boum.org/pipermail/tails-dev/2013-November/003999.html)"
msgstr ""
"[هدف ۲٫۰ += پشتیبانی از دستگاه‌های یواس‌بی به شکل "
"غیرقابل‌جداسازی؟](https://mailman.boum.org/pipermail/tails-"
"dev/2013-November/003999.html)"

#. type: Bullet: '  - '
msgid ""
"[Tor Launcher "
"extension](https://mailman.boum.org/pipermail/tails-dev/2013-November/004011.html)"
msgstr ""
"[افزونهٔ راه‌انداز تور](https://mailman.boum.org/pipermail/tails-"
"dev/2013-November/004011.html)"

#. type: Bullet: '  - '
msgid ""
"[WhisperBack launcher should give a hint on its "
"use](https://mailman.boum.org/pipermail/tails-dev/2013-November/004295.html)"
msgstr ""
"[راه‌انداز ویسپربک باید اشاره‌ای به فعالیت خود "
"بکند](https://mailman.boum.org/pipermail/tails-dev/2013-November/004295.html)"

#. type: Bullet: '  - '
msgid ""
"[Glossary for "
"contributors](https://mailman.boum.org/pipermail/tails-dev/2013-November/004353.html)"
msgstr ""
"[واژه‌نامه برای مشارکت‌کنندگان](https://mailman.boum.org/pipermail/tails-"
"dev/2013-November/004353.html)"

#. type: Title =
#, no-wrap
msgid "Funding\n"
msgstr "جذب سرمایه\n"

#. type: Bullet: '  - '
msgid ""
"ThinkPenguin.com donated hardware for us to investigate on UEFI support.  "
"Many thanks!"
msgstr ""
"ThinkPenguin.com برای بررسی پشتیبانی UEFI سخت‌افزارهایی به ما هدیه داد. تشکر "
"بسیار!"

#. type: Bullet: '  - '
msgid "We have been working on a proposal with sponsor Golf."
msgstr "در حال کار روی پیشنهادی برای اسپانسر گلف بوده‌ایم."

#. type: Title =
#, no-wrap
msgid "Outreach\n"
msgstr "توسعه\n"

#. type: Bullet: '  - '
msgid "We submitted a talk for 30C3 but it was rejected."
msgstr "یک ارائه برای 30C3 پیشنهاد دادیم که رد شد."

#. type: Bullet: '  - '
msgid ""
"2013-11-28: [Helping Human Rights Defenders to Communicate Securely: TAILS, "
"National Democratic Institute, "
"USA](http://www.coe.int/en/web/world-forum-democracy/lab4_) at the World "
"Forum for Democracy"
msgstr ""
"۲۰۱۳/۱۱/۲۸: [کمک به مدافعان حقوق بشر برای ارتباط امن‌تر: تیلز، موسسه "
"دموکراتیک ملی، آمریکا](http://www.coe.int/en/web/world-forum-democracy/lab4_)"
" در موسسهٔ دموکراتیک ملی"

#. type: Title =
#, no-wrap
msgid "Press and testimonials\n"
msgstr "اخبار و قدردانی‌ها\n"

#. type: Bullet: '  - '
msgid ""
"2013-11: The German-speaking ADMIN magazine [reviews "
"Tails](http://www.admin-magazin.de/Das-Heft/2013/11/Tails-0.20)."
msgstr ""
"۲۰۱۳/۱۱: مجلهٔ آلمانی‌زبان ADMIN [تیلز را بررسی کرده‌است](http://www.admin-"
"magazin.de/Das-Heft/2013/11/Tails-0.20)."
