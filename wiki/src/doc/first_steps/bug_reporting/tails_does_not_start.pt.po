# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2015-08-12 18:59+0300\n"
"PO-Revision-Date: 2015-07-07 22:59+0200\n"
"Last-Translator: Tails Developers <amnesia@boum.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Tails does not start\"]]\n"
msgstr "[[!meta title=\"Tails não inicia\"]]\n"

#. type: Plain text
msgid ""
"If Tails does not start properly, refer first to the [[known issues|/support/"
"known_issues]] page. Similar issues might have already been reported for the "
"same model of computer."
msgstr ""
"Se o Tails não inicia adequadamente, consulte primeiro a página de "
"[[problemas conhecidos|/support/known_issues]]. Problemas similares podem já "
"ter sido relatados para o mesmo modelo de computador."

#. type: Plain text
msgid ""
"Otherwise refer to the following sections, depending on whether or not the "
"[[boot menu|/doc/first_steps/startup_options#boot_menu]] appears when "
"starting Tails:"
msgstr ""
"Do contrário, consulte as sessões subsequentes, a depender se o [[menu de "
"inicialização|/doc/first_steps/startup_options#boot_menu]] aparece ou não na "
"inicialização do Tails."

#. type: Plain text
#, no-wrap
msgid "<a id=\"at_all\"></a>\n"
msgstr "<a id=\"at_all\"></a>\n"

#. type: Title -
#, no-wrap
msgid "Tails does not start at all\n"
msgstr "Tails não incia de jeito nenhum\n"

#. type: Plain text
#, fuzzy
#| msgid ""
#| "The following section applies if the [[boot menu|/doc/first_steps/"
#| "startup_options#boot_menu]] does not appears when starting Tails."
msgid ""
"The following section applies if the [[boot menu|/doc/first_steps/"
"startup_options#boot_menu]] does not appear when starting Tails."
msgstr ""
"A seção a seguir se aplica nos casos em que o [[menu de inicialização|/doc/"
"first_steps/startup_options#boot_menu]] não aparece na inicialização do "
"Tails."

#. type: Plain text
msgid ""
"Refer to the instructions on how to [[start Tails|doc/first_steps/"
"start_tails]], especially the section about BIOS settings."
msgstr ""

#. type: Plain text
#, fuzzy
#| msgid ""
#| "[[Send us an email|/support/talk]] including the following information:"
msgid ""
"If none of this works, [[send us an email|/support/talk]] including the "
"following information:"
msgstr ""
"[[Mande um email para nós|/support/talk]] incluindo as seguintes informações:"

#. type: Bullet: '0. '
msgid "Which version of Tails are you trying to start?"
msgstr "Qual versão do Tails você está tentando iniciar?"

#. type: Bullet: '0. '
msgid "Did you [[verify the ISO image|download#verify]]?"
msgstr "Você [[verificou a imagem ISO|download#verify]]?"

#. type: Bullet: '1. '
msgid "Which is the brand, and model of the computer?"
msgstr "Quais são a marca e o modelo do computador?"

#. type: Bullet: '2. '
msgid ""
"What exactly happens when trying to start? Report the complete error message "
"that appears on the screen, if any."
msgstr ""
"O que acontece exatamente quando você tenta inicializar? Relate a mensagem "
"de erro completa que aparece na tela, se aparecer alguma."

#. type: Bullet: '3. '
msgid ""
"From which media are you trying to start Tails: DVD, USB stick [[installed "
"manually|doc/first_steps/installation/manual]], USB stick [[installed with "
"Tails Installer|doc/first_steps/installation]], SD card? Keep in mind that, "
"we do not support any other installation method than the ones listed above."
msgstr ""
"Através de que mídia você está tentando inicializar o Tails: DVD, memória "
"USB [[instalada manualmente|doc/first_steps/installation/manual]], memória "
"USB [[instalado com o instalador do Tails|doc/first_steps/installation]], "
"cartão SD? Tenha em mente que nós não oferecemos suporte a métodos de "
"instalação que não sejam os listados acima."

#. type: Bullet: '4. '
msgid ""
"Have you been able to start Tails successfully on this computer before, from "
"another media, or with another version of Tails? If so, which ones?"
msgstr ""
"Você conseguiu iniciar o Talis com sucesso neste computador antes, a partir "
"de uma outra mídia ou com outra versão do Tails? Se sim, quais?"

#. type: Bullet: '5. '
msgid "Does the same media start successfully on other computers?"
msgstr "A mesma mídia inicializa com sucesso em outros computadores?"

#. type: Bullet: '6. '
msgid ""
"Have you been able to start Tails successfully on the same computer using "
"different installation methods? For example, it might start from a DVD but "
"not from a USB stick."
msgstr ""
"Você conseguiu iniciar o Tails com sucesso no mesmo computador usando "
"métodos diferentes de instalação? Por exemplo, ele consegue iniciar de um "
"DVD mas não de uma memória USB."

#. type: Bullet: '7. '
msgid "What installation method did you use to set up Tails?"
msgstr "Que método de instalação você usou para  configurar o Tails?"

#. type: Plain text
#, no-wrap
msgid "<a id=\"entirely\"></a>\n"
msgstr "<a id=\"entirely\"></a>\n"

#. type: Title -
#, no-wrap
msgid "Tails does not start entirely\n"
msgstr "Tails não inicia por completo\n"

#. type: Plain text
msgid ""
"The following section applies if the [[boot menu|/doc/first_steps/"
"startup_options#boot_menu]] appears but not [[Tails Greeter|/doc/first_steps/"
"startup_options/#tails_greeter]] when starting Tails."
msgstr ""
"A seção a seguir se aplica nos casos em que, na inicialização do Tails, o "
"[[menu de inicalização|/doc/first_steps/startup_options#boot_menu]] aparece "
"mas o [[Tails Greeter|/doc/first_steps/startup_options/#tails_greeter]] não."

#. type: Bullet: '1. '
msgid "In the graphical boot menu, press `TAB`."
msgstr "No menu gráfico de inicialização, pressione `TAB`."

#. type: Bullet: '2. '
msgid "Remove the `quiet` option from the boot command line."
msgstr "Remova a opção `quiet` da linha de comando de inicialização."

#. type: Bullet: '3. '
msgid "Add the `debug` and `nosplash` option."
msgstr "Adicione as opções `debug` e `nosplash`."

#. type: Bullet: '4. '
msgid ""
"Hopefully, this displays useful messages while starting Tails. You can then "
"include them in a bug report to be sent:"
msgstr ""
"Se tudo der certo, isso mostrará mensagens úteis durante a inicialização do "
"Tails. Você pode incluí-las no relatório de bug a ser enviado:"

#. type: Bullet: '   - '
msgid ""
"either using [[WhisperBack|/doc/first_steps/bug_reporting]] if you are able "
"to start Tails from another media,"
msgstr ""
"seja usando o [[WhisperBack|/doc/first_steps/bug_reporting]] se você "
"consegue iniciar o Tails através de outra mídia,"

#. type: Bullet: '   - '
msgid "either by [[sending us an email|/support/talk]]"
msgstr "ou [[nos mandando um email|/support/talk]]"

#. type: Bullet: '5. '
msgid ""
"If the error message is `/bin/sh: can't access tty; job control turned off` "
"followed by `(initramfs)`, then try removing the `live-media=removable` "
"option."
msgstr ""
"Se a mensagem de erro for `/bin/sh: can't access tty; job control turned "
"off` followed by `(initramfs)`, então tente remover a opção `live-"
"media=removable`."

#. type: Plain text
#, no-wrap
msgid "   <div class=\"caution\">\n"
msgstr "   <div class=\"caution\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"   <strong>When removing this option, if an adversary installed a fake\n"
"   Tails on an internal hard disk, then you will likely be starting\n"
"   this dangerous operating system instead of the genuine Tails that\n"
"   you intended to use.</strong>\n"
msgstr ""
"   <strong>Ao remover esta opção, caso um adversário tenha instalado uma versão falsa\n"
"   do Tails em um disco rígido interno, você estará inicializando\n"
"   este sistema operacional perigoso ao invés do Tails genuíno que\n"
"   você pretendia utilizar.</strong>\n"

#. type: Plain text
#, no-wrap
msgid "   </div>\n"
msgstr "   </div>\n"

#. type: Plain text
#, no-wrap
msgid ""
"   If removing `live-media=removable` allows you to start Tails, please\n"
"   report a bug as documented above: this allows us to improve the [[list\n"
"   of problematic USB sticks|support/known_issues#problematic-usb-sticks]].\n"
"   In this case, you should install Tails on another, better supported\n"
"   USB stick.\n"
msgstr ""
"  Se remover `live-media=removable` permitir que você incicialize o Tails, por favor\n"
"  relate um bug conforme documentado acima: isso nos permite melhorar a [[lista\n"
"  de dispositivos USB problemáticos|support/known_issues#problematic-usb-sticks]].\n"
"  Nesse caso, você deveria instalar o Tails em dispositivo USB diferente\n"
"  e melhor suportado.\n"

#~ msgid ""
#~ "If you are knowledgeable about BIOS configuration, you can also try the "
#~ "following:"
#~ msgstr ""
#~ "Se você tem conhecimento sobre configuração da BIOS, você também pode "
#~ "tentar o seguinte:"

#~ msgid "Disable Fast boot."
#~ msgstr "Desabilitar Inicialização rápida"

#~ msgid ""
#~ "If the computer is configured to start with legacy BIOS, try to configure "
#~ "it to start with UEFI. Else, if the computer is configured to start with "
#~ "UEFI, try to configure it to start with legacy BIOS. Try any of the "
#~ "following options if available:"
#~ msgstr ""
#~ "Se o computador estiver configurado para incializar pela BIOS, tente "
#~ "configurá-lo para iniciar com UEFI. Caso contrário, se o computador "
#~ "estiver configurado para iniciar com UEFI, tente configurá-lo para "
#~ "iniciar pela BIOS. Tente qualquer uma das seguintes opções caso estejam "
#~ "disponíveis:"

#~ msgid "Enable Legacy mode"
#~ msgstr "Habilite o modo Legacy"

#~ msgid "Disable Secure boot"
#~ msgstr "Desabilite a Inicialização Segura (Disable Secure Boot)"

#~ msgid "Enable CSM boot"
#~ msgstr "Habilite a inicialização via CSM (Enable CSM Boot)"

#~ msgid "Disable UEFI"
#~ msgstr "Desabilite o UEFI (Disable UEFI)"

#~ msgid "Try to upgrade your BIOS version."
#~ msgstr "Tente atualizar a versão da sua BIOS"
