# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2016-01-28 14:21+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Media appearances in 2016\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"* 2016-01: Several articles cover the release of Tails 2.0:\n"
"  - 2016-01-27: [Tails 2.0: Das Anonymisierungs-OS im neuen Look](http://www.heise.de/security/meldung/Tails-2-0-Das-Anonymisierungs-OS-im-neuen-Look-3085312.html) by Fabian A. Scherschel in heise (in German).\n"
"  - 2016-01-27: [Edward Snowden's OS of choice gets a major update](http://www.engadget.com/2016/01/27/edward-snowdens-os-of-choice-gets-a-major-update/) by Steve Dent in engadget.\n"
msgstr ""

#. type: Bullet: '* '
msgid ""
"2016-01-12: Tails is listed as best distro for privacy in [The Best Linux "
"Distros of 2016](http://www.linux.com/news/software/applications/878620-the-"
"best-linux-distros-of-2016/)  by Swapnil Bhartiya in Linux.com."
msgstr ""
